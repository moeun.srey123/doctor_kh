<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'internal_trailproject' );

/** MySQL database username */
define( 'DB_USER', 'internal_dbadmin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'u_,BT5Q~v^3S' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'dO3H~D{@}159::??z&aHxxP{O6#^Pwd-<P|7W8U(PSCUC)PWf So2^xah@OL,zHh' );
define( 'SECURE_AUTH_KEY',  '{;v8?N?08^<vi]vLsN(j,vBjwt|iwaR;;TVufSN6]VO4dAI7|w./c%(:a(T=.brO' );
define( 'LOGGED_IN_KEY',    'e=i-tMlWz}A0VPsS6:`Jlo94TpnxKjV`^G+fr5>cLRF4XAP5O)XFy2)dQC!O4omB' );
define( 'NONCE_KEY',        '*{_t0*&scpH1[}1@Ya,4$ESxGMl#1=YMC?o7eO;FGb4IGr &uYehM*`fxi>$;%3a' );
define( 'AUTH_SALT',        '1X$6IOUfxb<:,|sG^ke(jfkR0nvl,sBJ-{+5F *rq+w?|ml)iRnS>P8iv _s|ZP=' );
define( 'SECURE_AUTH_SALT', '(o*qT&CWfj>|-eFOEs[|j;V8Bm}4GTN?UV=jyoa<1udon!95~CM3FFC%|^47.qf1' );
define( 'LOGGED_IN_SALT',   'INxQY5]gkw.~}OR3 [qQA!C%nIwOrw~*d!>13sX#1( ~543uf?E)_oMv5@8oFWaI' );
define( 'NONCE_SALT',       'yti-op+P0)3$^Uuk@o/:I_U8]fh5)}2L^vMKx9jl8eC~H$Ikz]cs`1~aVv~k0%Oa' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
