$(document).ready(function(){


    // header menu
    $('.navbar-light .show-mobile .navbar-toggler').click(function(){
         $('.show-mobile .navbar-toggler-icon').toggle();
        if($('.navbar-toggler .header-icon').hasClass('d-none')) {
            $('.navbar-toggler .header-icon').removeClass('d-none');
        } else {
            $('.navbar-toggler .header-icon').addClass('d-none');
        }
    });


    $('.show-mobile .nav-link').addClass('container');
    //sidebar product
        var url = window.location;
    $('.flex-column .nav-link a[href="'+ url +'"]').parent().addClass('active');
    $('.flex-column .nav-link').filter(function() {
            return this.href == url;
    }).parent().addClass('active');
    //end sidebar product
    // contact form
    $('input[type="submit"]').addClass('btn btn-success');
    $('.center').slick({
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 4,
        infinite: false,
        responsive: [
            {
            breakpoint: 992,
            settings: {
                arrows: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            }
            },
            {
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            }
            },
            {
            breakpoint: 480,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            }
            }
        ]
    });
    $('.display-product').slick({
    // slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    infinite: false,
    responsive: [
            {
        breakpoint: 992,
        settings: {
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            // autoplay: true,
            // autoplaySpeed: 2000,
        }
        },
        {
        breakpoint: 768,
        settings: {
            arrows: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            // autoplay: true,
            // autoplaySpeed: 2000,
        }
        },
        {
        breakpoint: 480,
        settings: {
             arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
        }
        }
    ]
    })
});
//page doctor slide slick
$(function() {
    $('.contact_item').matchHeight();
});
//    Modal for buy form in single product
$(document).ready(function(){
    $("#exampleModalCenter").on("show.bs.modal", function(e) {
        var images = $('.img-pro').attr('src');
        var title_modal_product = $('.title-product-post').html();
        var product_id = $('.product_id').html();
        var date = $('.date_post').html();
        var price = $('.price').html();
        $('.modal-body input[type="number"]').val(1);
        $('.modal-body input[type="number"]').on('keyup mouseup', function () {
        var q = $('.modal-body input[type="number"]').val();
        var total = price.slice(1) * q;
        $(".modal-body .total").html( "&nbsp;"+"$"+total);
        }).trigger('mouseup');
    
        $(".modal-body .img_modal").attr('src',images );
        $(".modal-body .title_modal_product").html(title_modal_product);
        $(".modal-body .id_modal_product").html(product_id);
        $(".modal-body .date_modal_product").html(date);
        var $cancel = $('<button type="button" class="btn btn-cancel" data-dismiss="modal">Close</button>');
        $cancel.appendTo($(".modal-body .btn-order"));
    });
    $("#exampleModalCenter").on("hide.bs.modal", function(e) {
        $('.btn-cancel').remove();
    });
});

$(document).ready(function(){
    $('.slide-doctors').slick({
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 4,
        infinite: false,
        responsive: [
            {
            breakpoint: 992,
            settings: {
                arrows: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            }
            },
            {
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            }
            },
            {
            breakpoint: 480,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            }
            }
        ]
    });


})

$(document).ready(function(){
    $('.categories-doctor .nav-link').on('click',function (e) {
        e.preventDefault();
        var target = this.hash;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop':  $target.offset().top //no need of parseInt here
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });
});
