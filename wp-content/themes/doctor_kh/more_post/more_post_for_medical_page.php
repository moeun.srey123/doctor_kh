<?php 

    function misha_my_load_more_scripts() {

        global $wp_query;

        // In most cases it is already included on the page and this line can be removed
        wp_enqueue_script('jquery');

        // register our main script but do not enqueue it yet
        wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/more_post/functions.js', array('jquery') );

        // now the most interesting part
        // we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
        // you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
        wp_localize_script( 'my_loadmore', 'loadmore_params', array(
            'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
            'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
            'current_page' => get_query_var( 'paged', 1 ) ? get_query_var('paged', 1) : 1,
            'max_page' => $wp_query->max_num_pages
        ) );
        wp_enqueue_script( 'my_loadmore' );
    }
    add_action( 'wp_enqueue_scripts', 'misha_my_load_more_scripts' );




    function misha_loadmore_ajax_handler(){
        global $wp_query;
        
        // prepare our arguments for the query
        $args = json_decode( stripslashes( $_POST['query'] ), true );
        $paged= $_POST['page'] + 1; // we need next page to be loaded
        $args =array(
            'post_status' => 'publish',
            'post_type'  =>  'products',
            'posts_per_page' => 12 ,
            'paged' => $paged
        );
        query_posts( $args );

        if( have_posts() ) :
            // run the loop
            echo '<div  class="row product_all">';
            
            while( have_posts() ): the_post();

                // look into your theme code how the posts are inserted, but you can use your own HTML of course
                // do you remember? - my example is adapted for Twenty Seventeen theme
                echo '<div class="col-lg-4 col-md-4">';
                        echo '<a href="'.get_the_permalink().'">';
                            echo '<div class="product-content">';
                                echo '<img src="'.get_the_post_thumbnail_url().'" class="w-100">';
                                echo '<h6 class="title-post-product">'.get_the_title() .'</h6>';
                                echo '<h6 class="price-product">$'.get_field("price").'</h6>';
                            echo '</div>';
                        echo '</a>';
                    echo '</div>';
                // for the test purposes comment the line above and uncomment the below one
                // the_title();
            endwhile;
            echo '</div>';
            
        endif;
        die; // here we exit the script and even no wp_reset_query() required!
    }
    add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
    add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}


?>