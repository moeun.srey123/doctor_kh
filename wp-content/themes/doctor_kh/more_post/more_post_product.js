jQuery(function($){ // use jQuery code inside this to avoid "$ is not defined" error
	$('#btn-product-more-posts').click(function(){
		var button = $(this),
		    data = {
			'action': 'more-post-products',
			'query': more_post_products_para.posts, // that's how we get params from wp_localize_script() function
			'page' : more_post_products_para.current_page,
			'queried_object' : more_post_products_para.queried_object,
		};
		$.ajax({ // you can also use $.post here
			url : more_post_products_para.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); // change the button text, you can also add a preloader image
			},
			success : function( data ){
				if( data ) {
					button.text( 'More' ).before(data); // insert new posts
					more_post_products_para.current_page++;

					if ( more_post_products_para.current_page == more_post_products_para.max_page )
						button.remove(); // if last page, remove the button

					// you can also fire the "post-load" event here if you use a plugin that requires it
					// $( document.body ).trigger( 'post-load' );
				} else {
					button.remove(); // if no data, remove the button as well
				}
			}
		});
	});
});