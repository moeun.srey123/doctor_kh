<?php get_header();?>
    <div class="container-fluid  doctor-content">
        <div class="bg-light-gray">
            <div class="container">
                <h1 class="title_header_doctor text-uppercase">Specialty categories</h1>
                <div class="categories-doctor">
                     <?php  
                       $taxonomy_objects = get_object_taxonomies( 'doctors', 'doctor' ); 
                       foreach ( $taxonomy_objects as $taxonomy_slug => $taxonomy ){
                           $terms = get_terms( $taxonomy_slug, 'hide_empty=0' );
                           if ( !empty( $terms ) ) {
                            echo '<ul class="nav">';
                            echo '</a></li>';
                              foreach ( $terms as $term ) {
                                echo'<li class="nav-item "><a class="nav-link" href="'. get_permalink() .'#'. $term->slug.'" >';
                                echo $term->name;
                                echo '</a></li>';
                              }
                             echo '</ul>';
                            }
                       }
                        ?>
                </div>
            </div>
        </div>
        <div class="show-all-post-each-categories">
            <div class="each-taxonomy">
                <?php  
                $cat_args = array(
                    'hide_empty'    => 0, 
                );
                $terms = get_terms('doctor', $cat_args);
                foreach($terms as $taxonomy){?>
                    <div class="container" id="<?php echo $taxonomy->slug;?>">
                        <h1 class="title-taxonomy text-uppercase"><?php echo $taxonomy->name ?></h1>
                        <?php $term_slug = $taxonomy->slug;
                            $tax_post_args = array(
                                'post_type' => 'doctors',
                                'posts_per_page' => -1,
                                'order' => 'ASC',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'doctor',
                                        'field' => 'slug',
                                        'terms' => $term_slug
                                    )
                                )
                            );
                            $tax_post_qry = new WP_Query($tax_post_args);
                            if($tax_post_qry->have_posts()) :?>
                                <div class="row slide-doctors">
                                    <?php 
                                    while($tax_post_qry->have_posts()) :
                                        $tax_post_qry->the_post();
                                    ?>
                                        <div class="col-lg-6 p-doctor">
                                            <a href="<?php  echo get_permalink();?>"><img src="<?php echo get_the_post_thumbnail_url();?>" class="w-100 img-doctor"></a>
                                            <a href="<?php  echo get_permalink();?>"><h6 class="text-center title-post-doctor"><?php echo the_title();?></h6></a>
                                            <ul class="star">
                                            <?php for($i=0;$i<5;$i++){
                                                echo '<li><img src="'.get_template_directory_uri().'/assets/img/star-doctor.png"/> </li>';
                                            }?>
                                            </ul>
                                            <p class="text-center post-content"><?php echo wp_trim_words(get_field("skill"),5);?></p>
                                        </div>
                                    <?php
                                        endwhile; 
                                    ?>
                                </div>
                            <?php
                            else :
                                echo "No posts";
                            endif;
                            ?>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
<?php get_footer();?>