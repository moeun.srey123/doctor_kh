<?php get_header();?>
<div class="container media_product">
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <nav class="navbar sidebar navbar-expand-lg navbar-light bg-gray">
                 <h6 class="navbar-brand">PRODUCT CATEGORIES</h6>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon image_icon_sidebar"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <?php 
                         
                       $taxonomy_objects = get_object_taxonomies( 'products', 'medical-products' ); 
                       foreach ( $taxonomy_objects as $taxonomy_slug => $taxonomy ){
                           $terms = get_terms( $taxonomy_slug, 'hide_empty=0' );
                           if ( !empty( $terms ) ) {
                            echo '<ul class="nav flex-column" id="sidebar-product">';
                            echo'<li class="nav-item active"><a class="nav-link" href="'.get_page_link( 15 ).'">All Product';
                            echo '</a></li>';
                              foreach ( $terms as $term ) {
                                echo'<li class="nav-item "><a class="nav-link" href="'. get_term_link( $term ) .'">';
                                echo $term->name;
                                echo '</a></li>';
                              }
                             echo '</ul>';
                            }
                       }
                        ?>
                </div>
            </nav>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="row block-search">
                <div class="col-lg-5 col-md-12">
                    <h6 class="text-uppercase title_product_media">Our Product</h6>
                </div>
                <div class="col-lg-7 col-md-12">
                     <form class="search ml-auto" method="get" action="<?php echo home_url(); ?>" role="search">
                        <input type="text" class="search-field form-control " placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" aria-label="Search" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
                        <input type="hidden" name="post_type" value="products" />
                        <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_search.png"/> </span>
                    </form>
                </div>
            </div>
            <div id="ajax-posts"  class="row product_all">
                <?php 
                    echo '<div class="col-lg-12 col-md-12"><p class="show-all-products">Showing all '.wp_count_posts( 'products' )->publish.' results</p></div>';
                    $query_args = array(
                        'posts_per_page' => 12,
                        'post_type' => 'products',
                        'post_status' => 'publish',
                    );

                    $the_query = new WP_Query( $query_args );
                    // The Loop
                    if ( $the_query->have_posts() ) {
                        while ( $the_query->have_posts() ) {
                            $the_query->the_post();
                            echo '<div class="col-lg-4 col-md-4 col-6">';
                            echo '<div class="product-content">';
                            echo '<a href="'.get_the_permalink().'">';
                            echo '<img src="'.get_the_post_thumbnail_url().'" class="w-100 border-radius">';
                            echo '</a>';
                            echo '<a href="'.get_the_permalink().'">';
                            echo '<h6 class="title-post-product">'.get_the_title() .'</h6>';
                            echo '</a>';
                            echo '<h6 class="price-product">$'.get_field("price").'</h6>';
                            echo '</div>';
                            echo '</div>';
                        }
                        if (  $the_query->max_num_pages > 1 ){
                            echo '<div class="col-lg-12 col-md-12 text-center"><button id="loadmore" class="btn btn-success more">More</button></div>';
                        }
                    }
                    wp_reset_postdata();
                 ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>