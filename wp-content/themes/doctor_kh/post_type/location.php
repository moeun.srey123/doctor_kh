<?php 

    function post_type_Location() {
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Locationn', 'Post Type General Name', 'doctorkh' ),
            'singular_name'       => _x( 'Location', 'Post Type Singular Name', 'doctorkh' ),
            'menu_name'           => __( 'Location', 'doctorkh' ),
            'parent_item_colon'   => __( 'Parent Location', 'doctorkh' ),
            'all_items'           => __( 'All Location', 'Locatdoctorkhionkh' ),
            'view_item'           => __( 'View Location', 'doctorkh' ),
            'add_new_item'        => __( 'Add New Location', 'doctorkh' ),
            'add_new'             => __( 'Add New', 'doctorkh' ),
            'edit_item'           => __( 'Edit Location', 'doctorkh' ),
            'update_item'         => __( 'Update Location', 'doctorkh' ),
            'search_items'        => __( 'Search Location', 'doctorkh' ),
            'not_found'           => __( 'Not Found', 'doctorkh' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'doctorkh' ),
        );
        
    // Set other options for Custom Post Type   
        $args = array(
            'label'               => __( 'Locations', 'doctorkh' ),
            'description'         => __( 'Location news and reviews', 'doctorkh' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'excerpt', 'custom-fields' , 'thumbnail' ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           =>'dashicons-location-alt',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
        );    
        // Registering your Custom Post Type
        register_post_type( 'Locations', $args );
    
    }
    
    add_action( 'init', 'post_type_Location' , 0);

?>