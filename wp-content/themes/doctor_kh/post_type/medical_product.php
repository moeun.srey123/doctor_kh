<?php 

    function custom_post_type_product() {
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Product', 'Post Type General Name', 'doctorkh' ),
            'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'doctorkh' ),
            'menu_name'           => __( 'Product', 'doctorkh' ),
            'parent_item_colon'   => __( 'Parent Product', 'doctorkh' ),
            'all_items'           => __( 'All Product', 'doctorkh' ),
            'view_item'           => __( 'View Product', 'doctorkh' ),
            'add_new_item'        => __( 'Add New Product', 'doctorkh' ),
            'add_new'             => __( 'Add New', 'doctorkh' ),
            'edit_item'           => __( 'Edit Product', 'doctorkh' ),
            'update_item'         => __( 'Update Product', 'doctorkh' ),
            'search_items'        => __( 'Search Product', 'doctorkh' ),
            'not_found'           => __( 'Not Found', 'doctorkh' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'doctorkh' ),
        );
        
    // Set other options for Custom Post Type   
        $args = array(
            'label'               => __( 'products', 'doctorkh' ),
            'description'         => __( 'Product news and reviews', 'doctorkh' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-cart',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
        );    
        // Registering your Custom Post Type
        register_post_type( 'products', $args );
    
    }
    add_action( 'init', 'custom_post_type_product', 0 );
    //hook into the init action and call create_book_taxonomies when it fires
    add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
    
    //create a custom taxonomy name it topics for your posts
    
    function create_topics_hierarchical_taxonomy() {
    
        // Add new taxonomy, make it hierarchical like categories
        //first do the translations part for GUI
        
        $labels = array(
            'name' => _x( 'Medical Products', 'taxonomy general name' ),
            'singular_name' => _x( 'Medical Products', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Topics' ),
            'all_items' => __( 'All Topics' ),
            'parent_item' => __( 'Parent Topic' ),
            'parent_item_colon' => __( 'Parent Topic:' ),
            'edit_item' => __( 'Edit Topic' ), 
            'update_item' => __( 'Update Topic' ),
            'add_new_item' => __( 'Add New Topic' ),
            'new_item_name' => __( 'New Topic Name' ),
            'menu_name' => __( 'Medical Products' ),
        );    
        
        // Now register the taxonomy
        
        register_taxonomy('medical-products', array('products'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'medical-products' ),
        ));
        
    }
 
    add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
    function tsm_filter_post_type_by_taxonomy() {
        global $typenow;
        $post_type = 'products'; // change to your post type
        $taxonomy  = 'medical-products'; // change to your taxonomy
        if ($typenow == $post_type) {
            $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
            $info_taxonomy = get_taxonomy($taxonomy);
            wp_dropdown_categories(array(
                'show_option_all' => sprintf( __( 'All %s', 'textdomain' ), $info_taxonomy->label ),
                'taxonomy'        => $taxonomy,
                'name'            => $taxonomy,
                'orderby'         => 'name',
                'selected'        => $selected,
                'show_count'      => true,
                'hide_empty'      => true,
            ));
        };
    }
    /**
    * Filter posts by taxonomy in admin
    */
    add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
    function tsm_convert_id_to_term_in_query($query) {
        global $pagenow;
        $post_type = 'products'; // change to your post type
        $taxonomy  = 'medical-products'; // change to your taxonomy
        $q_vars    = &$query->query_vars;
        if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
            $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
            $q_vars[$taxonomy] = $term->slug;
        }
    }
?>