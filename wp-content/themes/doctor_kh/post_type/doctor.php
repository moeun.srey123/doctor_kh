<?php 

    function custom_post_type() {
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Doctor', 'Post Type General Name', 'doctorkh' ),
            'singular_name'       => _x( 'Doctor', 'Post Type Singular Name', 'doctorkh' ),
            'menu_name'           => __( 'Doctor', 'doctorkh' ),
            'parent_item_colon'   => __( 'Parent Doctor', 'doctorkh' ),
            'all_items'           => __( 'All Doctor', 'doctorkh' ),
            'view_item'           => __( 'View Doctor', 'doctorkh' ),
            'add_new_item'        => __( 'Add New Doctor', 'doctorkh' ),
            'add_new'             => __( 'Add New', 'doctorkh' ),
            'edit_item'           => __( 'Edit Doctor', 'doctorkh' ),
            'update_item'         => __( 'Update Doctor', 'doctorkh' ),
            'search_items'        => __( 'Search Doctor', 'doctorkh' ),
            'not_found'           => __( 'Not Found', 'doctorkh' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'doctorkh' ),
        );
        
    // Set other options for Custom Post Type   
        $args = array(
            'label'               => __( 'doctors', 'doctorkh' ),
            'description'         => __( 'Doctor news and reviews', 'doctorkh' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'excerpt', 'custom-fields' , 'thumbnail' ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-businessperson',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
        );    
        // Registering your Custom Post Type
        register_post_type( 'doctors', $args );
    
    }
    
    add_action( 'init', 'custom_post_type' , 0);

     //hook into the init action and call create_book_taxonomies when it fires
    add_action( 'init', 'create_doctor_taxonomy', 0 );
    
    //create a custom taxonomy name it Doctor for your posts
    
    function create_doctor_taxonomy() {
    
        // Add new taxonomy, make it hierarchical like categories
        //first do the translations part for GUI
        
        $labels = array(
            'name' => _x( 'Doctor', 'taxonomy general name' ),
            'singular_name' => _x( 'Doctor', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Doctor' ),
            'all_items' => __( 'All Doctor' ),
            'parent_item' => __( 'Parent Doctor' ),
            'parent_item_colon' => __( 'Parent Doctor:' ),
            'edit_item' => __( 'Edit Doctor' ), 
            'update_item' => __( 'Update Doctor' ),
            'add_new_item' => __( 'Add New Doctor' ),
            'new_item_name' => __( 'New Doctor Name' ),
            'menu_name' => __( 'Doctor' ),
        );    
        
        // Now register the taxonomy
        
        register_taxonomy('doctor', array('doctors'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'doctor' ),
        ));
        
    }
 
    add_action('restrict_manage_posts', 'filter_doctor_taxonomy');
    function filter_doctor_taxonomy() {
        global $typenow;
        $post_type = 'doctors'; // change to your post type
        $taxonomy  = 'doctor'; // change to your taxonomy
        if ($typenow == $post_type) {
            $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
            $info_taxonomy = get_taxonomy($taxonomy);
            wp_dropdown_categories(array(
                'show_option_all' => sprintf( __( 'All %s', 'textdomain' ), $info_taxonomy->label ),
                'taxonomy'        => $taxonomy,
                'name'            => $taxonomy,
                'orderby'         => 'name',
                'selected'        => $selected,
                'show_count'      => true,
                'hide_empty'      => true,
            ));
        };
    }
    /**
    * Filter posts by taxonomy in admin
    * @author  Mike Hemberger
    * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
    */
    add_filter('parse_query', 'convert_id_to_term_in_query_doctor');
    function convert_id_to_term_in_query_doctor($query) {
        global $pagenow;
        $post_type = 'doctors'; // change to your post type
        $taxonomy  = 'doctor'; // change to your taxonomy
        $q_vars    = &$query->query_vars;
        if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
            $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
            $q_vars[$taxonomy] = $term->slug;
        }
    }
?>