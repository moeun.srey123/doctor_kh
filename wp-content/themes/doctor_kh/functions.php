<?php 
require_once('bs4navwalker.php');
function add_theme_scripts() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );

    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array());
    wp_enqueue_style('slick-css', get_template_directory_uri() . '/assets/vendor/slick/slick.css', array());
    wp_enqueue_style('slick-theme-css', get_template_directory_uri() . '/assets/vendor/slick/slick-theme.css', array());

    wp_enqueue_script('jquery-3', get_template_directory_uri() . '/assets/js/jquery-3.4.1.slim.min.js', array());
    wp_enqueue_script('jquery-1', get_template_directory_uri() . '/assets/js/jquery-1.11.0.min.js', array());
    wp_enqueue_script('jquery-migrate', get_template_directory_uri() . '/assets/js/jquery-migrate-1.2.1.min.js', array());
    wp_enqueue_script('popper-js', get_template_directory_uri() . '/assets/js/popper.min.js', array());
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array());
    wp_enqueue_script('slick-js', get_template_directory_uri() . '/assets/vendor/slick/slick.min.js', array());
    wp_enqueue_script('jquery.matchHeight-js', get_template_directory_uri() . '/assets/js/jquery.matchHeight.js',array());
    wp_enqueue_script('script', get_template_directory_uri() . '/script.js', array());
}

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );
register_nav_menus( array(
            'primary_menu' => __( 'Primary' ),
            'footer_menu'  => __( 'Footer' ),
        ) );
add_theme_support('post-thumbnails'); 

/**
 * Implement the custom post type medical product.
 */
require get_template_directory() . '/post_type/medical_product.php';


/**
 * Implement the custom post type doctor.
 */
require get_template_directory() . '/post_type/doctor.php';

/**
 * Implement the button more post in page medical product.
 */
require get_template_directory() . '/more_post/more_post_for_medical_page.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 * More post for medical product
 */
require get_template_directory() . '/more_post/more_post_for_product.php';

/**
 * post type location
 */
require get_template_directory() . '/post_type/location.php';