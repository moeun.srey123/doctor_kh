        <footer class="bg-black">
            <div class="container ">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <p class="copy-right">Copyright © 2019 Doctor.KH Co.,Ltd</p>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <ul class="icon d-desktop">
                            <li><a href="https://www.facebook.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png" class="img-fluid"/> </a></li>
                            <li><a href="https://twitter.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png" class="img-fluid"/></a></li>
                            <li><a href="https://www.google.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/google.png" class="img-fluid"/></a></li>
                        </ul>
                         <ul class="icon d-mobile">
                            <li><a href="https://www.facebook.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png" class="img-fluid"/> </a></li>
                            <li><a href="https://twitter.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png" class="img-fluid"/></a></li>
                            <li><a href="https://www.google.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/google.png" class="img-fluid"/></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>