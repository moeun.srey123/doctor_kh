<?php get_header();?>
    <div class ="container post_doctor">
        <div class ="row">
            <div class="col-lg-12 col-md-12">
                <?php 
                    require get_template_directory() . '/assets/vendor/breadcrumbs/breadcrumb-doc.php';
                ?>
            </div>
            <div class ="col-lg-4 col-md-6">
                <img src = "<?php echo get_the_post_thumbnail_url();?>" class ="img-fluid w-100 border-radius">
            </div>
             <div class ="col-lg-8 col-md-6 info-doctor">
                <h6 class ="title-doctor-post"><?php echo the_title();?></h6>
                <ul class="star-doctor">
                    <?php
                        for($i=0;$i<5;$i++){
                            echo '<li><img src="'.get_template_directory_uri().'/assets/img/star-doctor.png"/></li>';
                        }
                    ?>
               </ul>
               <h6 class="position">Position &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo get_field('position');?></h6>
               <h6 class="gender">Gender &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo get_field('gender');?></h6>
               <h6 class="skill-title">Skill&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo get_field('skill');?></h6>
               <!--<p class="skill-detail"><?php echo get_field('skill');?></p>-->
               <div class="btn btn-success" >Make an Appointment</div>
            </div>
            <div class="col-lg-12 col-md-12 product_description">
                <h6 class="expertises text-uppercase">Expertises</h6>
                <p class="expertises-detail"><?php echo get_field('expertises');?></p>
            </div>
            <div class="col-lg-12 work-location">
            <h6 class="title-location text-uppercase">Work location</h6>
            <?php 
                $posts = get_field('work_location');
                if( $posts ): ?>
                <div class="row">
                    <?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
                        <div class="col-lg-8 col-md-8 col-7">
                            <h6 class="clinic-name"><?php echo get_the_title( $p->ID ); ?></h6>
                            <p class="address-clinic"><?php echo get_field('address_clinic', $p->ID);?></p>
                        </div>
                        <div class="col-lg-4 col-md-4 map col-5">
                                <?php echo get_field('map', $p->ID);?>
                        </div>
                    <?php endforeach; ?>
                </div>     
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php get_footer();?>

