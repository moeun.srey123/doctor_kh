<?php get_header();?>
    <div class="container">
        <h6 class="text-uppercase text-center title-contact"><?php the_title();?></h6>
        <div class="row p-contact-us">
            <div class="col-lg-3 col-md-6 col-12 text-center">
                <!--Address-->
                <div class="bg-light-gray content_item">
                    <img src="<?php echo get_field('image_address')['url'];?>"/>
                    <div class=" address">
                        <p><?php echo get_field('address');?></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12 text-center ">
                <!--Email-->
                <div class="bg-light-gray content_item">
                    <img src="<?php echo get_field('image_email')['url'];?>"/>
                    <div class="email">
                      
                        <?php 
                            for ($i=1;$i<=2;$i++){
                                $email = get_field('email_'.$i);?>
                                  <p><?php echo $email;?> </p>
                        <?php }?>
                       
                    </div>
                </div> 
            </div>
            <div class="col-lg-3 col-md-6 col-12 text-center ">
                <!--Phone-->
                <div class="bg-light-gray content_item">
                    <img src="<?php echo get_field('image-phone')['url'];?>"/>
                    <div class="phone">
                      
                        <?php 
                            for ($i=1;$i<=3;$i++){
                                $phone = get_field('phonenumber_'.$i);?>
                                  <p><?php echo $phone;?></p>
                        <?php }?>
                        
                    </div>
                </div>  
            </div>
            <div class="col-lg-3 col-md-6 col-12 ">
                <!--Social-->
                <div class="bg-light-gray content_item">
                    <p class="text-center" style="margin-bottom:0px!important"><img src="<?php echo get_field('image_social')['url'];?>" class="img-socail"/></p>
                    <div class="social">
                        <?php 
                            $link = get_field('facebook_name');
                            if( $link ): 
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                ?>
                               <p><a href="<?php echo esc_url( $link_url ); ?>">
                                <img src="<?php echo get_field('image_facebook')['url'];?>">
                                <?php echo esc_html( $link_title ); ?>
                                </a></p>
                        <?php endif; ?>
                        <?php 
                            $link = get_field('twitter_name');
                            if( $link ): 
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                ?>
                                <p><a href="<?php echo esc_url( $link_url ); ?>">
                                <img src="<?php echo get_field('image_twitter')['url'];?>">
                                <?php echo esc_html( $link_title ); ?>
                                </a></p>
                        <?php endif; ?>  
                        <?php 
                            $link = get_field('google_name');
                            if( $link ): 
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                ?>
                                <p><a href="<?php echo esc_url( $link_url ); ?>">
                                <img src="<?php echo get_field('image_google')['url'];?>">
                                <?php echo esc_html( $link_title ); ?>
                                </a>
                               </p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 map">
                <?php if ( have_posts() ) :
                        while ( have_posts() ) :the_post();
                ?>
                        <?php the_content();?>
                <?php   endwhile;
                    else :
                    echo 'Nothing found';
                    endif;
                ?>
            </div>
        </div>
    </div>
    <div class="container-fluid p-0 bg-light-gray">
        <div class="container p-contact-form contact_form">
            <?php echo get_field('form_mail');?>
        </div>
    </div>
<?php get_footer();?>