<?php get_header();?>
<div class="container about p-about">
    <h6 class="text-uppercase title-header"><?php the_title(); ?></h6>
    <div class="row">
        <div class="col-lg-12 col-md-12">
           <?php if ( have_posts() ) :
                    while ( have_posts() ) :the_post();
            ?>
                    <?php the_content();?>
            <?php   endwhile;
                else :
                echo 'Nothing found';
                endif;
            ?>
        </div>
        <div class="col-lg-6 col-md-12">
            <?php echo get_field('text_left');?>
        </div>
        <div class="col-lg-6 col-md-12 text-center">
             <?php $image = get_field('image_1'); ?>
            <img src="<?php echo $image['url']; ?>" alt="" class="img-fluid img-about-left">
            
        </div>
        <div class="col-lg-12 col-lg-12">
            <h6 class="title"><?php echo get_field('title'); ?></h6>
        </div>
        <div class="col-lg-12 col-lg-12">
            <?php echo get_field('detail_title'); ?>
        </div>
        <div class="col-lg-12 col-md-12 text-center">
             <?php $image = get_field('image_2'); ?>
            <img src="<?php echo $image['url']; ?>" alt="" class="img-fluid img-about-bottom">
            
        </div>
    </div>
</div>
<?php get_footer();?>