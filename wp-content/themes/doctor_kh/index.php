<?php get_header();?>
<div class="container-fluid p-0 bg-image" style="background-image:url('<?php echo esc_url(get_theme_mod('doctor_kh_home_page_image_background' )); ?> ');"> 
    <div class="text-home">
        <div class="bg-color">
            <div class="container">
                <div class = "row">
                    <div class = "col-lg-6 col-md-12">
                    <?php $homepage_title = get_theme_mod('doctor_kh_home_page_title' , '');
                    if(!empty($homepage_title)) { ?>
                        <h6 class="home_title"><?php echo $homepage_title; ?></h6>
                    <?php } ?>
                      <?php $homepage_description = get_theme_mod('doctor_kh_home_page_description' , '');
                    if(!empty($homepage_description)) { ?>
                        <p class="home_detail"><?php echo $homepage_description;?></p>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row m-service">
        <div class="col-lg-12 text-uppercase"><h6 class ="title-service">services</h6></div>
        <?php
            $frontpage = new WP_Query([
            'cat' => 3,
            ]); ?>
            <?php if($frontpage->have_posts()):?>
            <?php while($frontpage->have_posts()):$frontpage->the_post();  ?>
        <div class="col-lg-4 col-md-12 detail-service">
            <img src="<?php the_post_thumbnail_url();?>"  class="img-service"/>
            <h6 class="sub-title-service"><?php the_title();?></h6>
            <?php the_content();?>
        </div>
        <?php 
        endwhile;
         endif;?>
    </div>
</div>
<div class="container-fluid p-0 bg-gray doctor">
    <h6 class="text-center title-doctor text-uppercase">Doctors</h6>
    <div class="container">
        <div class="row center">
            <?php
                $recent_posts = wp_get_recent_posts(array('post_type'=>'doctors', 'posts_per_page'=>8));
                foreach( $recent_posts as $recent ){
                    echo '<div class="col-lg-6 slide p-doctor">';
                    echo '<a href="' . get_permalink($recent["ID"]) . '">';
                    echo '<img src="'.get_the_post_thumbnail_url($recent["ID"]).'"class="w-100 img-doctor">';
                    echo '</a>';
                    echo '<a href="' . get_permalink($recent["ID"]) . '">';
                    echo '<h6 class="text-center title-post-doctor">'. $recent["post_title"].'</h6>';
                    echo '</a>';
                    echo '<ul class="star">';
                    for($i=0;$i<5;$i++){
                         echo '<li><img src="'.get_template_directory_uri().'/assets/img/star-doctor.png"/></li>';
                    }
                    echo  '</ul>';
                    echo '<p class="text-center post-content">'.wp_trim_words(get_field("skill" , $recent["ID"]),5).'</p>';
                    echo '</div>';
                }
            ?>
        </div>
    </div>
</div>
<div class="container p-home">
    <div class="row product">
        <div class="col-lg-12">
            <h6 class="text-center title-product text-uppercase">Medical Products</h6>
        </div>
        <?php
            $recent_posts = wp_get_recent_posts(array('post_type'=>'products', 'posts_per_page'=>8));
            foreach( $recent_posts as $recent ){
                echo '<div class="col-lg-3">';
                echo '<div class="product-content">';
                echo '<a href="' . get_permalink($recent["ID"]) . '">';
                echo '<img src="'.get_the_post_thumbnail_url($recent["ID"]).'" class="w-100 border-radius">';
                echo '</a>';
                echo '<a href="' . get_permalink($recent["ID"]) . '">';
                echo '<h6 class="title-post-product">'. $recent["post_title"].'</h6>';
                echo '</a>';
                echo '<h6 class="price-product">$'.get_field("price", $recent["ID"]).'</h6>';
                echo '</div>';
                echo '</div>';
            }
        ?>
        <div class="col-lg-12 btn-product text-center">
            <!--<button type="button" class="btn btn-success">Success</button>-->
            <a href="<?php echo get_page_link( 15 )?>" class="link-more btn btn-success">More</a>
        </div>
    </div>
    <div class="row product-slide">
        <div class="col-lg-12">
            <h6 class="text-center title-product text-uppercase">Medical Products</h6>
        </div>
        <div class="col-lg-12 display-product">
            <?php
            $recent_posts = wp_get_recent_posts(array('post_type'=>'products', 'post_per_page'=>8));
            foreach( $recent_posts as $recent ){
                echo '<div class="">';
                echo '<div class="product-content">';
                echo '<a href="' . get_permalink($recent["ID"]) . '">';
                echo get_the_post_thumbnail($recent["ID"],'full' , array( 'class' => 'img-fluid border-radius w-100' ));
                echo '</a>';
                echo '<a href="' . get_permalink($recent["ID"]) . '">';
                echo '<h6 class="text-center title-post-product">'. $recent["post_title"].'</h6>';
                echo '</a>';
                echo '<h6 class="text-center price-product">$'.get_field("price", $recent["ID"]).'</h6>';
                echo '</div>';
                echo '</div>';
            }
        ?>
        </div>
        <div class="col-lg-12 btn-product text-center">
            <!--<button type="button" class="btn btn-success">Success</button>-->
            <a href="<?php echo get_page_link( 15 )?>" class="link-more btn btn-success">More</a>
        </div>
    </div>
</div>

<?php get_footer();?>
