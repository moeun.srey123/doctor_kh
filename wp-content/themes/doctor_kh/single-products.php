<?php get_header();?>
    <div class ="container post_product">
        <div class ="row">
            <div class="col-lg-12 col-md-12">
                <?php 
                    require get_template_directory() . '/assets/vendor/breadcrumbs/breadcrumb-pro.php';
                ?>
            </div>
            <div class ="col-lg-6">
                <img src = "<?php echo get_the_post_thumbnail_url();?>" class ="w-100 img-pro border-radius">
            </div>
             <div class ="col-lg-6 info-product">
                <h6 class ="title-product-post"><?php echo the_title();?></h6>
               <?php 
                    $product_id = get_field("product_id");
                    if(!empty($product_id)) { ?>
                        <h6 class="product_id">Product ID &nbsp;&nbsp;: <?php echo  $product_id; ?></h6>
                    <?php } 
                    $stock = get_field("stock");
                    if(!empty($stock)) { ?>
                        <h6 class="stock">Stock &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo  $stock; ?></h6>
                    <?php } 
                    $price = get_field("price");
                    if(!empty($price)) { ?>
                        <h6 class="price">$<?php echo  $price; ?></h6>
                    <?php } 
                ?>
                <h6 class="date_post d-none ">Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo get_the_date(); ?></h6>
                <div class="btn btn-success buy" data-toggle="modal" data-target="#exampleModalCenter">Buy Now</div>
            </div>
            <div class="col-lg-12 product_description">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                    the_content();
                    endwhile; else: ?>
                    <p>Sorry, no posts matched your criteria.</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12">
                     <h5 class="modal-title text-center" id="exampleModalLongTitle">Buyer Information</h5>
                </div>
            </div>
            <?php echo do_shortcode( '[contact-form-7 id="259" title="Order Contact"]' ); ?>
        </div>
        </div>
    </div>
    </div>
<?php get_footer();?>

