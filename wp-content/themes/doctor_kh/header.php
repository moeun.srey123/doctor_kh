<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">
        <META NAME="ROBOTS" CONTENT="INDEX, NOFOLLOW">
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <meta charset="<?php bloginfo( 'charset' ); ?>">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
    </head>
    <body  <?php body_class(); ?>>
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-header">
            <div class="show-daktop">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/doctor_kh.png"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <?php
                    wp_nav_menu([
                        'menu'            => 'primary_menu',
                        'theme_location'  => 'primary_menu',
                        'container'       => 'div',
                        'container_id'    => 'bs4navbar',
                        'container_class' => 'collapse navbar-collapse',
                        'menu_id'         => false,
                        'menu_class'      => 'navbar-nav ml-auto',
                        'depth'           => 2,
                        'fallback_cb'     => 'bs4navwalker::fallback',
                        'walker'          => new bs4navwalker()
                    ]);
                    ?>
                </div>
            </div>
            <div class="show-mobile">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/doctor_kh.png"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                         <span class="header-icon d-none"></span>
                    </button>
                </div>
                <hr class="border-header fixed-line shadow-sm">
                <div class="container-fluid p-0 bg-light-gray">
                    <?php
                    wp_nav_menu([
                        'menu'            => 'primary_menu',
                        'theme_location'  => 'primary_menu',
                        'container'       => 'div',
                        'container_id'    => 'bs4navbar',
                        'container_class' => 'collapse navbar-collapse',
                        'menu_id'         => false,
                        'menu_class'      => 'navbar-nav ml-auto',
                        'depth'           => 2,
                        'fallback_cb'     => 'bs4navwalker::fallback',
                        'walker'          => new bs4navwalker()
                    ]);
                    ?>
                </div>
            </div>
        </nav>
        <hr class="border-header fixed-line shadow-sm b-daktop">
    