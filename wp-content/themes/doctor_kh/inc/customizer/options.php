<?php

$wp_customize->add_panel( 'theme_option_panel', 
	array(
		'title'			=> __( 'Theme Options', 'doctor-kh' ),
		'description'	=> __( 'Customization options for Doctor KH Theme', 'doctor-kh' ),
		'priority'		=> 10	
	) 
);

// Load Options
require_once trailingslashit( get_template_directory() ) . '/inc/customizer/customizer-options/option-homepage.php';