<?php

// Section - Other Section
$wp_customize->add_section( 'doctor_kh_header_section',
    array(
        'title'      => esc_html__( 'Homepage Option', 'doctor-kh' ),
        'priority'   => 100,
        'capability' => 'edit_theme_options',
        'panel'      => 'theme_option_panel',
    )
);

// Setting - Home Page Title
$wp_customize->add_setting( 'doctor_kh_home_page_title',
    array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
    )
);

$wp_customize->add_control( 'doctor_kh_home_page_title',
    array(
        'label'    			=> esc_html__( 'Home Page Title', 'doctor-kh' ),
        'section'  			=> 'doctor_kh_header_section',
        'type'     			=> 'text',
    )
);

// Setting - Home Page Description
$wp_customize->add_setting( 'doctor_kh_home_page_description',
    array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
    )
);

$wp_customize->add_control( 'doctor_kh_home_page_description',
    array(
        'label'    			=> esc_html__( 'Home Page Description', 'doctor-kh' ),
        'section'  			=> 'doctor_kh_header_section',
        'type'     			=> 'textarea',
    )
);

// Setting - Home Page Image Background
$wp_customize->add_setting( 'doctor_kh_home_page_image_background',
    array(
        'default'           => '',
        'sanitize_callback' => 'doctor_kh_sanitize_image',
    )
);

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'logo',
           array(
               'label'      => __( 'Upload a logo', 'theme_name' ),
               'section'    => 'doctor_kh_header_section',
               'settings'   => 'doctor_kh_home_page_image_background',
           )
       )
   );