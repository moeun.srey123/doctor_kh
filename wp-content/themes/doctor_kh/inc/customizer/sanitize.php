<?php
/**
 * Sanitization Functions
 *
 * @package doctor_kh
 */

if ( ! function_exists( 'doctor_kh_sanitize_select' ) ) :

	/**
	 * Sanitize select.
	 *
	 * @since 1.0.0
	 *
	 * @param mixed                $input The value to sanitize.
	 * @param WP_Customize_Setting $setting WP_Customize_Setting instance.
	 * @return mixed Sanitized value.
	 */
	function doctor_kh_sanitize_select( $input, $setting ) {

		// Ensure input is clean.
		$input = sanitize_text_field( $input );

		// Get list of choices from the control associated with the setting.
		$choices = $setting->manager->get_control( $setting->id )->choices;

		// If the input is a valid key, return it; otherwise, return the default.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );

	}

endif;

if ( ! function_exists( 'doctor_kh_sanitize_checkbox' ) ) :

	/**
	 * Sanitize checkbox.
	 *
	 * @since 1.0.0
	 *
	 * @param bool $checked Whether the checkbox is checked.
	 * @return bool Whether the checkbox is checked.
	 */
	function doctor_kh_sanitize_checkbox( $checked ) {

		return ( ( isset( $checked ) && true === $checked ) ? true : false );

	}

endif;


if ( ! function_exists( 'doctor_kh_sanitize_positive_integer' ) ) :

	/**
	 * Sanitize positive integer.
	 *
	 * @since 1.0.0
	 *
	 * @param int $input Number to sanitize.
	 * @param WP_Customize_Setting $setting WP_Customize_Setting instance.
	 * @return int Sanitized number; otherwise, the setting default.
	 */
	function doctor_kh_sanitize_positive_integer( $input, $setting ) {

		$input = absint( $input );

		// If the input is an absolute integer, return it.
		// otherwise, return the default.
		return ( $input ? $input : $setting->default );

	}

endif;


if ( ! function_exists( 'doctor_kh_sanitize_image' ) ) :

	/**
	* Image sanitization callback example.
	*
	* Checks the image's file extension and mime type against a whitelist. If they're allowed,
	* send back the filename, otherwise, return the setting default.
	*
	* - Sanitization: image file extension
	* - Control: text, WP_Customize_Image_Control
	* 
	* @see wp_check_filetype() https://developer.wordpress.org/reference/functions/wp_check_filetype/
	*
	* @param string               $image   Image filename.
	* @param WP_Customize_Setting $setting Setting instance.
	* @return string The image filename if the extension is allowed; otherwise, the setting default.
	*/
	function doctor_kh_sanitize_image( $image, $setting ) {
		/*
		* Array of valid image file types.
		*
		* The array includes image mime types that are included in wp_get_mime_types()
		*/
		$mimes = array(
			'jpg|jpeg|jpe' => 'image/jpeg',
			'gif'          => 'image/gif',
			'png'          => 'image/png',
			'bmp'          => 'image/bmp',
			'tif|tiff'     => 'image/tiff',
			'ico'          => 'image/x-icon'
		);
		// Return an array with file extension and mime_type.
		$file = wp_check_filetype( $image, $mimes );
		// If $image has a valid mime_type, return it; otherwise, return the default.
		return ( $file['ext'] ? $image : $setting->default );
	}

endif;