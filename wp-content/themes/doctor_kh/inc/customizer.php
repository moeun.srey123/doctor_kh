<?php
/**
 * Doctor KH Theme Customizer
 *
 * @package doctor_kh
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function doctor_kh_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	/**
	 * Load sanitize.php
	 */
	require_once trailingslashit( get_template_directory() ) . '/inc/customizer/sanitize.php';

	/**
	 * Load options.php
	 */
	require_once trailingslashit( get_template_directory() ) . '/inc/customizer/options.php';
	

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'doctor_kh_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'doctor_kh_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'doctor_kh_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function doctor_kh_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function doctor_kh_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function doctor_kh_customize_preview_js() {
	wp_enqueue_script( 'doctor-kh-customizer', get_template_directory_uri() . '/inc/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'doctor_kh_customize_preview_js' );
