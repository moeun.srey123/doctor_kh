<ul class="breadcrumd-product">
    <li class="item-home">
        <a class="bread-link bread-home" href="<?php echo get_home_url(); ?>" title="Home">Home</a></li>
    <li class="separator separator-home"> 
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/next_breadcrumd.png"> 
    </li>
    <li class="item-cat item-custom-post-type-products">
        <a class="bread-cat bread-custom-post-type-products" href="<?php echo get_page_link( 15 )?>" title="Medical Product">
        Medical Product</a>
    </li>
    <li class="separator"> 
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/next_breadcrumd.png"> 
    </li>
    <?php 
        // if()
        $terms = get_the_terms( get_the_ID(), 'medical-products' );
            if(! empty( $terms )){
                foreach ( $terms as $term ) {
                    echo '<li class="item-cat"><a href="'.get_term_link($term).'">'.$term->name.'</a></li>';
                }
            }
    ?>
</ul>