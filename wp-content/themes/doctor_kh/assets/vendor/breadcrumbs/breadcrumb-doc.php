<ul class="breadcrumd-product">
    <li class="item-cat item-custom-post-type-products">
        <a class="bread-cat bread-custom-post-type-products" href="<?php echo get_page_link( 13 )?>" title="Doctor">
        Doctor</a>
    </li>
    <li class="separator"> 
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/next_breadcrumd.png"> 
    </li>
    <?php 
        // if()
        $terms = get_the_terms( get_the_ID(), 'doctor' );
            if(! empty( $terms )){
                foreach ( $terms as $term ) {
                    echo '<li class="item-cat">'.$term->name.'</li>';
                }
            }
    ?>
</ul>